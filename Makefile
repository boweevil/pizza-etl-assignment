DB_HOST ?= localhost
DB_PORT ?= 5432
DB_NAME ?= torqata
DB_USER ?= postgres
DB_PASS ?= example
DB_CONN ?= PGPASSWORD=$(DB_PASS) psql -h $(DB_HOST) -p $(DB_PORT) -U $(DB_USER) --quiet

# Test environment handling
.PHONY: build_test_env load_test_env_data stop_test_env drop_test_env_data up down run_etl_test
start_test_env: up
load_test_env_data: create_db load_customer_table load_api_orders_data
stop_test_env: drop_test_env_data down
drop_test_env_data: drop_customer_orders_table drop_customer_table drop_api_orders_data drop_db

up: create_db_directory
	@docker compose up api db adminer -d

down:
	@docker compose down

run_etl_test:
	@docker compose run etl \
		&& docker compose rm --force etl

create_db_directory:
	@mkdir -p data/db/
	@chmod 700 data/db/

view_db_logs:
	@docker logs $(shell docker ps | awk '/pizza-etl-assignment-.*db/ {print $$1}')


# Customer table
.PHONY: reset_customer_table drop_customer_table load_customer_table create_db
reset_customer_table: drop_customer_table load_customer_table

drop_customer_table:
	@$(DB_CONN) -d $(DB_NAME) \
		-c 'DROP TABLE IF EXISTS customer;'

load_customer_table:
	@$(DB_CONN) -d $(DB_NAME) \
		-f data/customer-2021-05-15.sql

create_db:
	@$(DB_CONN) -c 'CREATE DATABASE $(DB_NAME);'

drop_db:
	@$(DB_CONN) -c 'DROP DATABASE $(DB_NAME);'


# Customer orders table
.PHONY: drop_customer_orders_table
drop_customer_orders_table:
	@$(DB_CONN) -d $(DB_NAME) -c 'DROP TABLE IF EXISTS customer_orders;'


# API
.PHONY: reset_api_data drop_api_orders_data
reset_api_data: drop_api_orders_data load_api_orders_data

drop_api_orders_data:
	@echo 'DROP TABLE IF EXISTS orders;' | sqlite3 api/api_data.db

.PHONY: load_api_orders_data load_api_orders_transaction1 load_api_orders_transaction2 load_api_orders_transaction3 load_api_orders_transaction4 load_api_orders_transaction5
load_api_orders_data:
	@cat data/orders-2021-05-15.sql | sqlite3 api/api_data.db

load_api_orders_transaction1:
	@cat data/orders-transaction1.sql | sqlite3 api/api_data.db

load_api_orders_transaction2:
	@cat data/orders-transaction2.sql | sqlite3 api/api_data.db
