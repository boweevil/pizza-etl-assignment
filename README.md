# Pizza tool

## Dependencies

- A Linux or MacOS shell environment.  This was built on an M1 MacBook Pro.  I ran into a few issues I had to work around due to the Arm architecture.  These issues should not affect anyone on an x86 system.  This was not tested on Windows using the WSL.
- Docker: All tests are run inside Docker containers using `docker compose`.
- Make: I have used `GNU Make 3.81`.
- Psql: The Postgres client.  I have used `psql (PostgreSQL) 13.2`.
- Sqlite3: I have used version `3.32.3`.
- Pipenv: The python modules listed in the `Pipfile` are required to run the Ansible playbook.  You can manually install these dependencies if you do not want to use Pipenv.
- The other tools are built inside containers have Dockerfile to handled configuring their dependecies.  So, no further action should be required.  Running locally will require installing the dependencies manually.  Inspect the corrisponding `Dockerfile` for each tool to determine these dependencies.


## The ETL solution

```
etl
├── Dockerfile
├── __init__.py
├── __main__.py
├── config.py
├── customer_data_provider.py
├── customer_orders_data_provider.py
├── data_loader.py
├── data_warehouse_handler.py
├── orders_data_provider.py
├── population_data_provider.py
└── requirements.txt
```

The code executes the following steps.

1. Check for the existence of the `customer_orders` table in the data warehouse.
    - If the table does not exist, proceed to step 4.
    - If the table exists, proceed to step 2.
2. Retrieve the `last_order_id` from that table.
3. Send a get request to the API for all transactions newer than the `last_order_id`.  This is, of course, assuming that the API supports this request.  If the API can only return all transactions, this tool can be extended to handle breaking this data down manually.
4. If no new transactions are found, exit the ETL process.
    - If new transactions are found, proceed to step 5.
5. Pull customer data from the database into a data frame.
6. Join orders and customer data on the `customer_id` column into a new data frame called, `transactions_df`.
7. Load `transactions_df` data into the `customer_orders` table in the data warehouse.

The code uses a file, `config.py`, for connection strings and API keys.  In actual implementation, these secrets should be stored in a tool like AWS Secretsmanager or Hashicorp's Vault.  The code can be extended to handle the retrieval of these secrets at runtime or the deployment process can retrieve these values and deploy them to the target environment.

This tool can be extended to manage loading the required data to a reporting database (not featured here).  Using argparse, the desired task could be called at runtime.  Alternatively, a separate script, using the same libraries developed here, could be written for loading of the reporting data every day at 2:00 am.  It could be run as frequently as is desired.

NOTE: There is a block of commented code for obtaining data on adult populations by state from the census.gov API (https://www.census.gov/data/developers/data-sets.html).  This code should probably be run in a separate process, at much lengthier intervals, and put into a separate table in the data warehouse. However, I have included it here to show how it can be done.  This data could be used in the dashboard to compare customer locales and sales data with the population data to look for trends.  This insight could help with targeting marketing.

_Given more time, I would definitely add testing and extend the tool to report statuses to something like Slack, or maybe integrate with PagerDuty?  I would also like to extend the tool to manage the reporting data loading changes mentioned above._

## Infrastructure

```
infrastructure
├── backend.tf
├── environments
│   └── dev.tfvars
├── main.tf
├── providers.tf
├── templates
│   └── ansible_inventory.yml
└── variables.tf
```

The terraform code in the `infrastructure` directory creates the following assets in AWS:

- VPC
    - 2 public subnets
    - 2 private subnets
- Bastion server in the public subnet with an associated security group.  Used for accessing the ETL instance and/or the RDS instance listed below.
- ETL server in the private subnet with an associated security group
- Postgres RDS with an associated security group. To be used for a reporting database or even a data warehouse if so desired but it should be tuned and configured by a database administrator for such a task.
- An Ansible inventory file for use for provisioning this environment with Ansible.  It will be rendered in `provisioning/inventory.yml`.

_Given more time, I would add log groups, and implement monitoring and alerting through CloudWatch or some other tool._

## Provisioning

```
provisioning
├── ansible.cfg
├── main.yml
├── molecule
│   └── default
│       ├── converge.yml
│       ├── molecule.yml
│       └── verify.yml
├── requirements.yml
├── templates
│   ├── config.py
│   ├── etl.service
│   └── etl.timer
└── variables.yml
```

The ansible code in the `provisioning` directory prepares an Ubuntu target instance to execute the ETL process on 10-minute intervals controlled by a `systemd` timer.  The code includes a `molecule` scenario for testing the playbook against Ubuntu 18.04 and Ubuntu 20.04 using docker containers as targets.

_Given more time, I would like to add roles to provision users, harden the operating system in accordance with the company's security standards, and add built-in testing to the `molecule` framework._

## Some points on my testing environment:

* A single Postgres database hosts the RDBMS data source mentioned in the assignment and serves as the data warehouse.  In the actual implementation, it would likely be wise to use a columner database such as AWS Redshift or Snowflake as the data warehouse.

* Docker compose (see `docker-compose.yml`) creates the testing containers and exposes the required ports to the host OS.  A `Makefile` is used to manage this environment.


## Test environment contents:

- db: A postgres database server hosting the customer data
- adminer: A GUI admin interface for the postgres database
- api: A server running a simple Flask API serving up the orders data
- etl: A container in which the ETL process is run

### Testing

_Python dependencies_

- Using Pipenv, install the python dependencies into a virtual environment.  Alternatively, install the Ansible and Molecule dependencies listed in the `Pipfile`.

```
pipenv install
```

_The ETL testing environment_

- Start the API, DB and Adminer instances.

```
make start_test_env
```

- Verify that the database server is ready to accept connections.

```
make view_db_logs

# OUTPUT should end with the following when it is ready
2021-05-17 13:23:55.140 UTC [1] LOG:  database system is ready to accept connections
```

- After the instances are ready, load the data into the testing environment.

```
make load_test_env_data
```

- OPTIONAL: Log into the Adminer UI to follow along with the changes.  http://localhost:8080/
    - Select `PostgreSQL` from the `System` dropdown.
    - Enter `db` in the `Server` field.
    - Enter `postgres` in the `Username` field.
    - Enter `example` in the `Password` field.
    - Enter `torqata` in the `Database` field.
    - Click `Login`

- Back on the command line, run the ETL process.

```
make run_etl_test

# OUTPUT
Transactions data
     order_id customer_id        type  qty  retail_price            order_date               name                      address            city state zip_code
0           1  76-3411687     supreme    6         12.98  2020-05-15T00:29:04Z        Kermie Larn  6092 Lakewood Gardens Plaza  San Bernardino    CA    92405
1           2  50-8276624     supreme    6         12.98  2020-05-15T18:24:53Z   Isabel Constance          415 Luster Crossing        Hartford    CT    06145
2           3  26-9641718   pepperoni    7         11.98  2020-05-16T09:34:49Z         Ayn Fortey                  42 Haas Way      Washington    DC    20520
3           4  91-3403630  meat lover    1         12.98  2020-05-16T19:12:43Z  Northrop Stoddard             67 Gateway Plaza         Chicago    IL    60630
4           5  58-6172758     supreme    8         12.98  2020-05-17T02:48:57Z   Jeanette Marxsen               4466 Tony Pass     Minneapolis    MN    55428
..        ...         ...         ...  ...           ...                   ...                ...                          ...             ...   ...      ...
495       496  54-7836555     supreme    3         12.98  2020-11-11T10:04:57Z        Niels Dufer   83307 Messerschmidt Circle  Virginia Beach    VA    23464
496       497  42-0595571   pepperoni    5         11.98  2020-11-11T20:31:14Z        Morse Canet          85009 Farwell Point           Macon    GA    31296
497       498  66-7032543   pepperoni    8         11.98  2020-11-12T01:16:00Z       Madel Pattle             32 Cordelia Road        Richmond    VA    23260
498       499  05-0362133   pepperoni    5         11.98  2020-11-13T05:59:25Z       Celka Gudgen          1 Blackbird Terrace        Richmond    VA    23293
499       500  36-3914837   pepperoni   10         11.98  2020-11-13T08:14:42Z        Ferdy Eakle            1 Transport Place        Pasadena    CA    91131

[500 rows x 11 columns]

Running data load...
Data load complete.
```

- Notice from the output that it has loaded a data frame of 500 rows of transactions into the `customer_orders` table.  OPTIONAL: Select the newly created `customer_orders` table in the Adminer UI to see the loaded data.
- Run the ETL process again to see that no new transactions are found.

```
make run_etl_test

# OUTPUT
Getting last order_id
Last order: 500
Using API URL: http://api:5003/orders?last_order_id=500
{"orders": []}

No new transactions found.
```

- Load more transactions.

```
make load_api_orders_transaction1
```

- Run the ETL process.

```
make run_etl_test

# OUTPUT
Transactions data
     order_id customer_id        type  qty  retail_price            order_date             name                address         city state zip_code
0         501  64-5939858   pepperoni    8         11.98  2020-11-13T14:05:52Z   Drud Medgewick       413 Rigney Place      Hampton    VA    23668
1         502  66-5585464      veggie    9          9.98  2020-11-13T17:32:01Z  Conrad Scotland      731 American Road      Trenton    NJ    08603
2         503  85-5471115  meat lover    1         12.98  2020-11-13T18:29:50Z  Marthena Sheber   34032 Cardinal Alley  Tallahassee    FL    32309
3         504  31-8851617   pepperoni    4         11.98  2020-11-14T07:54:10Z     Aurelea Munt          1 Raven Court     Gastonia    NC    28055
4         505  92-0228848      cheese    9         10.98  2020-11-14T15:33:54Z     Missy Dyster      43715 Buell Trail        Tyler    TX    75705
..        ...         ...         ...  ...           ...                   ...              ...                    ...          ...   ...      ...
245       746  23-6705543   pepperoni   10         11.98  2021-02-12T04:36:35Z    Rori Betteson       88 Cherokee Lane  San Antonio    TX    78250
246       747  42-3302373   pepperoni    5         11.98  2021-02-12T09:44:09Z      Henri Nesby  7012 Maple Wood Court      Gilbert    AZ    85297
247       748  25-6856644      cheese   10         10.98  2021-02-12T11:29:46Z    Bunny Scriver         77 Carey Place    Pensacola    FL    32520
248       749  87-3976014   pepperoni    8         11.98  2021-02-12T17:30:50Z      Alysa Tenny  6997 Steensland Plaza       Albany    NY    12247
249       750  37-4881807      cheese    2         10.98  2021-02-12T17:33:07Z  Marlena Stanyon    326 Washington Pass      El Paso    TX    79955

[250 rows x 11 columns]

Running data load...
Data load complete.
```

- Notice from the output that it has loaded another 250 rows of transactions into the `customer_orders` table.  OPTIONAL: View the `customer_orders` table in the Adminer UI to see the loaded data.

_The Ansible playbook_

- Leave the ETL testing environment running and load the next set of transactions.  This will prepare another 250 rows of transactions in the API.

```
make load_api_orders_transaction2
```

- Change directory to `provisioning`.

```
cd provisioning
```

- Install the required Ansible roles.

```
pipenv run ansible-galaxy install -r requirements.yml
# OR the following if you manually installed the python dependencies.
ansible-galaxy install -r requirements.yml
```

- Build the `molecule` testing infrastructure.

```
pipenv run molecule create
# OR
molecule create
```

- List the created instances to verify they are ready.

```
pipenv run molecule list
# OR
molecule list

# OUTPUT
DEBUG    Validating schema /Users/jason/Desktop/torqata_assignment/provisioning/molecule/default/molecule.yml.
INFO     Running default > list
                        ╷             ╷                  ╷               ╷         ╷
  Instance Name         │ Driver Name │ Provisioner Name │ Scenario Name │ Created │ Converged
╶───────────────────────┼─────────────┼──────────────────┼───────────────┼─────────┼───────────╴
  pizza-tool-ubuntu1804 │ docker      │ ansible          │ default       │ true    │ false
  pizza-tool-ubuntu2004 │ docker      │ ansible          │ default       │ true    │ false
                        ╵             ╵                  ╵               ╵         ╵
```

- Run the converge process to execute the playbook on the `molecule` docker containers.

```
pipenv run molecule converge
# OR
molecule converge
```

- Log into one of the running containers to test the process or check on the status of the `systemd` timer.

```
pipenv run molecule login -h pizza-tool-ubuntu2004
# OR
molecule login -h pizza-tool-ubuntu2004
```

- Check the logs for the timer. hit Ctrl + C to stop following the logs.  Keep in mind, this runs every ten minutes.

```
journalctl -u etl.service -f
```

- You can also run the process manually.  Consecutive executions will yield not new transactions.  If the output below shows no new transactions, try the `journalctl` command above to see if the timer ran.   OPTIONAL: View the `customer_orders` table in the Adminer UI to see the loaded data.

```
cd /etl
python3 __main__.py

# OUTPUT
Transactions data
     order_id customer_id        type  qty  retail_price            order_date                name                    address              city state zip_code
0         751  56-7235958     supreme    9         12.98  2021-02-12T19:38:50Z    Darlene Marquese      92 Glacier Hill Place          Marietta    GA    30061
1         752  06-9229300  meat lover    1         12.98  2021-02-12T22:41:02Z          Zeb Neasam    42 Blue Bill Park Court             Tampa    FL    33647
2         753  83-9010726      cheese    7         10.98  2021-02-12T23:28:41Z      Bernhard Guppy      10 Morningstar Avenue           Atlanta    GA    30386
3         754  19-3887832      veggie    1          9.98  2021-02-13T02:37:42Z     Marcellus Ronan           8611 Sloan Trail  Port Saint Lucie    FL    34985
4         755  11-0540493     supreme    2         12.98  2021-02-13T08:42:55Z  Karlens Carmichael     13 Sutteridge Crossing          Honolulu    HI    96825
..        ...         ...         ...  ...           ...                   ...                 ...                        ...               ...   ...      ...
245       996  14-2698910      cheese    8         10.98  2021-05-12T20:06:06Z    Pollyanna Garnar  17897 Westerfield Terrace        Washington    DC    20425
246       997  74-4209898  meat lover    3         12.98  2021-05-13T00:47:00Z     Reinaldo Ireson               83 Utah Lane           Atlanta    GA    31190
247       998  12-8780777      cheese    3         10.98  2021-05-13T19:53:27Z          Ina Travis            67315 Towne Way       Los Angeles    CA    90081
248       999  42-0044647      cheese    9         10.98  2021-05-13T21:33:01Z          Josh Lates          43 Rutledge Plaza          Portland    OR    97255
249      1000  08-9519621     supreme    8         12.98  2021-05-14T21:45:45Z       Nat Squirrell                 0 1st Road            Conroe    TX    77305

[250 rows x 11 columns]
```

_Destroy the test environment_

- Log out of the molecule testing environment by hitting `Ctrl + d` or typing `exit`.
- Destroy the molecule testing environment.

```
pipenv run molecule destroy
# OR
molecule destroy
```

- Change back to the parent directory.

```
cd ..
```

- Run the make commands to reset the data in the test environment.

```
make stop_test_env
```

_Given more time, I would like to further simplify the testing process and improve the documentation._


## Closing statement

I have opted for a traditional solution in running the ETL process on an Ec2 instance because it provides the most flexibilty in maintainenance and troubleshooting of the overall process, and it allowed me to showcase my skills with Ansible.  This is one example of how this infrastructure could look.  If the runtime of the ETL process isn't too long, it could be run in AWS Lambda.  It could also be run in a container in AWS ECS as a scheduled task.  Logs can be shipped to a centralized logging system such as ElasticSearch, DataDog, or Graylog.  I have really enjoyed working on this project and am grateful for your consideration.  I look forward to hearing from you.