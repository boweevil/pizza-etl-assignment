from json import dumps

from flask import Flask, request
from flask_jsonpify import jsonify
from flask_restful import Api, Resource
from sqlalchemy import create_engine

db_connect = create_engine('sqlite:///api_data.db')
app = Flask(__name__)
api = Api(app)


class Orders(Resource):
    def get(self):
        last_order_id = request.args.get('last_order_id')
        conn = db_connect.connect()
        if last_order_id:
            query = conn.execute(
                f"SELECT * FROM orders WHERE order_id > {last_order_id}")
            result = {'orders': [dict(zip(tuple(query.keys()), i))
                                 for i in query.cursor]}
            return result
        query = conn.execute("SELECT * FROM orders")
        result = {'orders': [dict(zip(tuple(query.keys()), i))
                             for i in query.cursor]}
        return result


api.add_resource(Orders, '/orders')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5003')
