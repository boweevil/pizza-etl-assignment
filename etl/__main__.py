import config
from customer_data_provider import CustomerDataProvider
from customer_orders_data_provider import CustomerOrdersDataProvider
from data_loader import DataLoader
from data_warehouse_handler import DataWarehouseHandler
from orders_data_provider import OrdersDataProvider
from population_data_provider import PopulationDataProvider
import sqlalchemy


def main():
    # population_df = PopulationDataProvider(config.population_api_key).as_df
    # print("Adult population by state")
    # print(population_df.head())
    # print()

    db_engine = sqlalchemy.create_engine(
        config.db_connection_string
    )

    data_warehouse_handler = DataWarehouseHandler(db_engine)
    last_customer_orders_order_id = data_warehouse_handler.get_last_order_id()
    print(f"Last order: {last_customer_orders_order_id}")

    orders = OrdersDataProvider(config.api_url, last_customer_orders_order_id)
    print(orders.as_json)
    if orders.as_df.empty:
        print('No new transactions found.')
        print()
        return

    print('New transactions found.  Processing...')
    transactional_customer_ids = orders.get_orders_customer_ids()
    print('Orders data')
    print(orders.as_df.tail(15))
    print()

    customers = CustomerDataProvider(
        db_engine,
        transactional_customer_ids
    )
    print('Customer data')
    print(customers.as_df)
    print()

    transactions_df = orders.get_orders_transactions_df(customers.as_df)
    print('Transactions data')
    print(transactions_df)
    print()

    data_loader = DataLoader(
        db_engine,
        transactions_df,
        data_warehouse_handler._customer_orders
    )
    data_loader.load()
    print()


if __name__ == "__main__":
    main()
