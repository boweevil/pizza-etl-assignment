import pandas as pd


class CustomerDataProvider:
    def __init__(self, db_engine, transactional_customer_ids):
        self._db_engine = db_engine
        self._transactional_customer_ids = tuple(transactional_customer_ids)
        self.as_df = self.get_data_frame()

    def get_data_frame(self):
        db_connection = self._db_engine.connect()
        try:
            return pd.read_sql_query(
                "SELECT * FROM customer WHERE customer_id IN {}".format(
                    self._transactional_customer_ids),
                con=self._db_engine)
        finally:
            db_connection.close()
