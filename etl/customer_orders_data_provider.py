import pandas as pd


class CustomerOrdersDataProvider:
    def __init__(self, db_engine):
        self._db_engine = db_engine
        self.as_df = self.get_data_frame()

    def get_data_frame(self):
        db_connection = self._db_engine.connect()
        try:
            return pd.read_sql_query(
                "SELECT * FROM customer_orders",
                con=self._db_engine
            )
        except BaseException:
            pass
        finally:
            db_connection.close()
