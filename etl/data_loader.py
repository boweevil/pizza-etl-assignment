class DataLoader:
    def __init__(self, db_engine, data_frame, table):
        self._db_engine = db_engine
        self._data_frame = data_frame
        self._table = table

    def load(self):
        db_connection = self._db_engine.connect()
        print('Running data load...')
        try:
            return self._data_frame.to_sql(
                self._table, db_connection, if_exists='append', index=False)
        except ValueError:
            print(ValueError)
        except Exception:
            print(Exception)
        else:
            print(f"{self._table} has been created.")
        finally:
            db_connection.close()
            print('Data load complete.')
