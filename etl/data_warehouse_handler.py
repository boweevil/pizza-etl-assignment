import sqlalchemy

import pandas as pd


class DataWarehouseHandler:
    def __init__(self, db_engine):
        self._db_engine = db_engine
        self._customer_orders = "customer_orders"
        self.customer_orders_table_exists = self.check_if_customer_orders_table_exists()

    def check_if_customer_orders_table_exists(self):
        inspect_db = sqlalchemy.inspect(self._db_engine)
        db_connection = self._db_engine.connect()
        customer_orders_table_exists = inspect_db.dialect.has_table(
            db_connection, self._customer_orders
        )
        db_connection.close()
        return customer_orders_table_exists

    def get_last_order_id(self):
        if self.customer_orders_table_exists:
            db_connection = self._db_engine.connect()
            print("Getting last order_id")
            try:
                last_order = pd.read_sql_query(
                    f"SELECT * FROM {self._customer_orders} ORDER BY order_id DESC LIMIT 1",
                    con=self._db_engine)
                return last_order['order_id'].values[0]
            finally:
                db_connection.close()
            return None
