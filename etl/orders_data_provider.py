import json

import requests

import pandas as pd


class OrdersDataProvider:
    def __init__(self, api_url, last_customer_orders_order_id):
        self._last_customer_orders_order_id = last_customer_orders_order_id
        self._api_url = self.get_orders_url(api_url)
        self.as_json = self.get_orders_data()
        self.as_df = self.get_data_frame()

    def get_orders_url(self, api_url):
        if self._last_customer_orders_order_id:
            api_url = f"{api_url}?last_order_id={self._last_customer_orders_order_id}"
        print(f"Using API URL: {api_url}")
        return api_url

    def get_orders_data(self):
        return requests.get(self._api_url).text

    def get_data_frame(self):
        data = json.loads(self.as_json)['orders']
        return pd.DataFrame.from_dict(data)

    def get_orders_customer_ids(self):
        return self.as_df['customer_id'].tolist()

    def get_orders_transactions_df(self, customer_df):
        orders_transactions_df = self.as_df.join(
            customer_df.set_index('customer_id'),
            on='customer_id')
        return orders_transactions_df
