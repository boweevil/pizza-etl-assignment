import json

import requests

import pandas as pd


class PopulationDataProvider:
    def __init__(self, api_key):
        self._api_key = api_key
        self._api_url = (
            'https://api.census.gov/data/2019/pep/charagegroups' +
            '?get=NAME,POP&AGEGROUP=29' +
            '&for=state:*' +
            f"&key={self._api_key}"
        )
        self.as_json = self.get_population_data()
        self.as_df = self.get_data_frame()

    def get_population_data(self):
        return requests.get(self._api_url).json()

    def get_data_frame(self):
        return pd.DataFrame(self.as_json[1:], columns=self.as_json[0])
