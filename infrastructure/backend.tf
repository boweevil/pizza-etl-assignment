terraform {
  backend "s3" {
    profile        = "example-org"
    region         = "us-east-1"
    bucket         = "example-org-tfstate"
    key            = "pizza-tool/terraform.tfstate"
    dynamodb_table = "example-org-pizza-tool-tfstate"
  }
}
