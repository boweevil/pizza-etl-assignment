profile         = "dev"
region          = "us-east-1"
cidr            = "10.0.0.0/27"
azs             = ["us-east-1a", "us-east-1b"]
private_subnets = ["10.0.0.0/29", "10.0.0.16/29"]
public_subnets  = ["10.0.0.8/29", "10.0.0.24/29"]
public_allow_list = [
  "10.0.0.0/32",
  "10.1.0.0/32",
  "10.2.0.0/32",
]
db_user     = "dbuser"
db_password = "dbpassword"
