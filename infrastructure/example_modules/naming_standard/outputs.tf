output "vpc" {
  description = "Naming convention for VPCs."
  value       = "${var.application}-${var.environment}"
}

output "sg" {
  description = "Naming convention for security groups."
  value       = "${var.application}-sg-${var.environment}"
}

output "role" {
  description = "Naming convention for IAM roles.  Include instance task if provided."
  value = var.task != null ? (
    "${var.application}-${var.task}-${var.environment}"
    ) : (
    "${var.application}-role-${var.environment}"
  )
}

output "ec2" {
  description = "Naming convention for ec2 instances.  Include instance task if provided."
  value = var.task != null ? (
    "${var.application}-${var.task}-${var.environment}"
    ) : (
    "${var.application}-ec2-${var.environment}"
  )
}

output "rds" {
  description = "Naming convention for RDS clusters."
  value       = "${var.application}-rds-${var.environment}"
}
