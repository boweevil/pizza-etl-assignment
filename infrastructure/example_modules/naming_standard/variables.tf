variable "application" {
  type = string
}

variable "environment" {
  type = string
}

variable "task" {
  type    = string
  default = null
}
