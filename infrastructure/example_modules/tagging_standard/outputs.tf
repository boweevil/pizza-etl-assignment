locals {
  organization    = "torqata"
  cost_tag        = "${local.organization}:cost"
  identity_tag    = "${local.organization}:identity"
  cost_center_tag = { "${local.cost_tag}:center" = var.center }
  product_tag     = { "${local.cost_tag}:product" = var.product }
  application_tag = { "${local.identity_tag}:application" = var.application }
  environment_tag = { "${local.identity_tag}:environment" = var.environment }
  terraform_tag   = { "${local.identity_tag}:terraform" = true }
}

output "tags" {
  value = merge(
    local.cost_center_tag,
    local.product_tag,
    local.application_tag,
    local.environment_tag,
    local.terraform_tag,
  )
}

output "cost_center_tag" { value = local.cost_center_tag }
output "product_tag" { value = local.product_tag }
output "application_tag" { value = local.application_tag }
output "environment_tag" { value = local.environment_tag }
