variable "application" {
  type        = string
  description = "Name of the application"
  default     = null
}

variable "environment" {
  type        = string
  description = "The environment of the target infrastructure"
  default     = null
}

variable "center" {
  type        = string
  description = "The cost center of the target infrastructure"
  default     = null
}

variable "product" {
  type        = string
  description = "The product of the target infrastructure"
  default     = null
}
