locals {
  application = "pizza-tool"
  environment = terraform.workspace
  center      = "PDD"
  product     = "Pizza Data"
}

module "naming_standard" {
  # Theoretical module for maintaining a naming standard for the organization
  # The module should be in a separate repo to be versioned and utilized by
  # all Terraform code within the organization.
  # source = "git@github.com:example-org/naming_standard.git?ref=v0.0.6"
  source = "./example_modules/naming_standard/"

  application = local.application
  environment = local.environment
}

module "tagging_standard" {
  # Theoretical module for maintaining a tagging standard for the organization
  # The module should be in a separate repo to be versioned and utilized by
  # all Terraform code within the organization.
  # source = "git@github.com:example-org/tagging_standard.git?ref=v0.0.8"
  source = "./example_modules/tagging_standard/"

  application = local.application
  environment = local.environment
  center      = local.center
  product     = local.product
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = module.naming_standard.vpc
  tags = module.tagging_standard.tags

  cidr               = var.cidr
  azs                = var.azs
  private_subnets    = var.private_subnets
  public_subnets     = var.public_subnets
  enable_nat_gateway = true
  enable_vpn_gateway = false
}

module "bastion_instance_sg" {
  source  = "terraform-aws-modules/security-group/aws//modules/ssh"
  version = "~> 3.0"

  name = module.naming_standard.sg
  tags = module.tagging_standard.tags

  vpc_id              = module.vpc.vpc_id
  ingress_cidr_blocks = var.public_allow_list
}

module "bastion_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name = module.naming_standard.ec2
  tags = module.tagging_standard.tags

  instance_count              = 1
  ami                         = "ami-xxxxxxxxxxxxxxxxx"
  instance_type               = "t3.micro"
  key_name                    = "key_name"
  monitoring                  = true
  vpc_security_group_ids      = [module.bastion_instance_sg.this_security_group_id]
  subnet_id                   = module.vpc.public_subnets[0]
  associate_public_ip_address = true

  root_block_device = [
    {
      delete_on_termination = true
      volume_size           = 16
      volume_type           = "gp3"
    }
  ]
}

module "etl_instance_sg" {
  source  = "terraform-aws-modules/security-group/aws//modules/ssh"
  version = "~> 3.0"

  name = module.naming_standard.sg
  tags = module.tagging_standard.tags

  vpc_id              = module.vpc.vpc_id
  ingress_cidr_blocks = [var.cidr]
}

module "etl_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name = module.naming_standard.ec2
  tags = module.tagging_standard.tags

  instance_count              = 1
  ami                         = "ami-xxxxxxxxxxxxxxxxx"
  instance_type               = "t3.micro"
  key_name                    = "key_name"
  monitoring                  = true
  vpc_security_group_ids      = [module.etl_instance_sg.this_security_group_id]
  subnet_id                   = module.vpc.private_subnets[0]
  associate_public_ip_address = true

  root_block_device = [
    {
      delete_on_termination = true
      volume_size           = 50
      volume_type           = "gp3"
    }
  ]
}

module "db_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name = module.naming_standard.sg
  tags = module.tagging_standard.tags

  vpc_id              = module.vpc.vpc_id
  ingress_cidr_blocks = var.private_subnets
  ingress_rules = [
    "postgresql-tcp",
  ]
  egress_rules = ["all-all"]
}

module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  name       = module.naming_standard.rds
  tags       = module.tagging_standard.tags
  identifier = module.naming_standard.rds

  engine                              = "postgres"
  engine_version                      = "13.2"
  family                              = "mysql5.7"
  major_engine_version                = "5.7"
  instance_class                      = "db.t2.large"
  allocated_storage                   = 5
  username                            = var.db_user
  password                            = var.db_password
  maintenance_window                  = "Mon:00:00-Mon:03:00"
  backup_window                       = "03:00-06:00"
  monitoring_interval                 = "30"
  monitoring_role_name                = module.naming_standard.role
  create_monitoring_role              = true
  deletion_protection                 = true
  port                                = "3306"
  subnet_ids                          = module.vpc.private_subnets
  vpc_security_group_ids              = [module.db_sg.this_security_group_id]
  iam_database_authentication_enabled = true

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8mb4"
    },
    {
      name  = "character_set_server"
      value = "utf8mb4"
    }
  ]

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]
}

output "bastion_instance_public_ip" {
  value = module.bastion_instance.public_ip
}

output "etl_instance_private_ip" {
  value = module.etl_instance.private_ip
}

resource "local_file" "ansible_inventory" {
  depends_on = [module.bastion_instance, module.etl_instance]
  content = templatefile("${path.module}/templates/ansible_inventory.yml",
    {
      bastion_instance_public_ip = module.bastion_instance.public_ip
      etl_instance_private_ip    = module.etl_instance.private_ip
    }
  )
  filename = "${path.module}/../provisioning/inventory.yml"
}
