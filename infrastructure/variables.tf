variable "profile" {
  type        = string
  description = "AWS profile in which to create the infrastructure."
  default     = null
}

variable "region" {
  type        = string
  description = "AWS region in which to create the infrastructure."
  default     = null
}

variable "cidr" {
  type        = string
  description = "CIDR range for the new VPC."
  default     = null
}

variable "azs" {
  type        = list(any)
  description = "List of availability zones in which to create the subnets."
  default     = null
}

variable "private_subnets" {
  type        = list(any)
  description = "List of private subnets"
  default     = null
}

variable "public_subnets" {
  type        = list(any)
  description = "List of public subnets"
  default     = null
}

variable "public_allow_list" {
  type        = list(any)
  description = "List of organization IP public addresses to allow access to the bastion server."
  default     = null
}

variable "db_user" {
  type        = string
  description = "User name of the RDS admin account."
  default     = null
}

variable "db_password" {
  type        = string
  description = "Password for the RDS admin account."
  default     = null
}
